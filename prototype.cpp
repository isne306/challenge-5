﻿#include <iostream>
#include <string>

using namespace std;

void CheckPalindrome(string &word,bool &check)
{
	for(int i=0;i<word.length();i++)
  	{
  		if(word[i]==word[word.length()-i-1])
  		{
  			check = true;
  			break;
  		}
  		else
  		{
  			check = false;
  			break;
  		}
  	}
}

void delSpace(string &word)
{
	for (int i=0; i<word.length();i++) 
	{
     	if(word[i] == ' ')
        word.erase(i, 1);
  	} 
}

void delExc(string &word)
{
	for (int i=0; i<word.length();i++) 
	{
     	if(word[i] == '[124567890-=!@#$%^&*()_+]')
        word.erase(i,1);
  	}
}

void convert(string &word) 
{
 for (int i=0; i<word.length();i++) 
 {
  word[i] = tolower(word[i]);
 }
}


int main ()
{
	string word;
	bool check=true;
	locale loc;
	cout << "Words: ";
	getline(cin,word);

	//Delete Space
	delSpace(word);
	//Delete Exclamation mark
	delExc(word);
	//convert uppercase to lowercase 
  	convert(word);
 	//CheckPalindrome
  	CheckPalindrome(word,check);




  	if (check == true )
  	{
  		cout << word << " is Palindrome" << endl;
  	}
  	else
  	{
  		cout << word << " is not Palindrome" << endl;
  	}

  	return 0;

}